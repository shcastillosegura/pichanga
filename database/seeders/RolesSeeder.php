<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_role = new Role();
        $admin_role->slug = 'admin';
        $admin_role->name = 'admin';
        $admin_role->save();

        $super_role = new Role();
        $super_role->slug = 'supervisor';
        $super_role->name = 'supervisor';
        $super_role->save();

        $user_role = new Role();
        $user_role->slug = 'user';
        $user_role->name = 'User';
        $user_role->save();

        $guest_role = new Role();
        $guest_role->slug = 'guest';
        $guest_role->name = 'Invitado';
        $guest_role->save();



        $user = new User();
        $user->name = 'admin';
        $user->email = 'admin@email.com';
        $user->password = bcrypt('aveces');
        $user->save();
        $user->roles()->attach($admin_role);

        $user = new User();
        $user->name = 'supervisor';
        $user->email = 'super@email.com';
        $user->password = bcrypt('aveces');
        $user->save();
        $user->roles()->attach($super_role);


        $user = new User();
        $user->name = 'user1';
        $user->email = 'user@email.com';
        $user->password = bcrypt('aveces');
        $user->save();
        $user->roles()->attach($user_role);

        $user = new User();
        $user->name = 'invitado';
        $user->email = 'guest@email.com';
        $user->password = bcrypt('aveces');
        $user->save();
        $user->roles()->attach($guest_role);





    }
}
